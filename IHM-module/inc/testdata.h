#ifndef TESTDATA_H
#define TESTDATA_H


#include "Structure_type.h"
#include <iostream>

class TestData
{
public:
    TestData(){}

    TestData(   int                 m_testId,
                std::string         m_testName,
                std::string         m_testCategory,
                std::string         m_testDescription,
                std::string         m_testResultCause,
                std::string         m_timestamp,
                test_protocol       m_ethernetProtocol,
                result              m_testResult,
                bool                m_resultIsReady);

    ~TestData();


    int getTestId() const;
    void setTestId(int testId);

    std::string getTestName() const;
    void setTestName(const std::string &testName);

    std::string getTestDescription() const;
    void setTestDescription(const std::string &testDescription);

    result getTestResult() const;
    void setTestResult(const result &testResult);

    std::string getTestResultCause() const;
    void setTestResultCause(const std::string &testResultCause);

    std::string getTimestamp() const;
    void setTimestamp(const std::string &timestamp);

    bool getResultIsReady() const;
    void setResultIsReady(bool resultIsReady);

    test_protocol getEthernetProtocol() const;
    void setEthernetProtocol(test_protocol &ethernetProtocol);

    std::string getTestCategory() const;
    void setTestCategory(const std::string &testCategory);

private:

    int                 m_testId =0;              // test case ID
    std::string         m_testName;            // test case name
    std::string         m_testCategory;        // test case name
    std::string         m_testDescription;     // test case description
    std::string         m_testResultCause;     // the cause of the verdict/result
    std::string         m_timestamp;           // the test execution timestamp
    test_protocol       m_ethernetProtocol;    // the Ethernet protocol
    result              m_testResult;          // the actual test result
    bool                m_resultIsReady;       // the test result is ready or not
};

#endif // TESTDATA_H
