#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMenu>
#include <QFile>
#include <QObject>
#include <QTableWidget>
#include <iostream>
#include <QString>
#include <QFuture>
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <QTimer>
#include <functional>
#include <QThread>
#include "corexmlparse.h"
#include <QMessageBox>
#include <QDirIterator>
#include <QXmlStreamReader>
#include "IComModule.h"
#include <iostream>
#include <ctime>
#include <QColorDialog>
#include <QTextBrowser>

TestDataCreator testDataCreator;

std::function<void(bool)> on_connection_update1;

bool STATUS;
using namespace std::placeholders;

class ComModuleInstance : public IComModule
{
};
ComModuleInstance ComModuleInstance;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
                                          ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    xmlFile = new CoreXmlParse(ui);
    xmlFile->open();
    xmlFile->startParse();
    ui->progressBar->setValue(0) ;
    ui->treeView->setIndentation(30);
    QObject::connect(this,&MainWindow::update_tester_stat,this,&MainWindow::on_connection_update);


     //ui->treeView->expandAll();

//    generate_report();
}
void MainWindow::generate_report(){
    /*time_t rawtime;
      struct tm * timeinfo;
      char buffer[80];

      time (&rawtime);
      timeinfo = localtime(&rawtime);

      strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
      std::string str(buffer);
     std::string         timestamp=str;*/

        testDataCreator.printTestList();

        Reporting *report = new Reporting(testDataCreator);
        QString filename = QDate::currentDate().toString("'C:\\Users\\uid1321\\Desktop\\rep\\frontend-com-module\\IHM-module\\doc\\Report_'yyyy_MM_dd'.html'");
        std::string path= filename.toStdString();
        report->setHtmlPath(path);
        report->generateHtmlReport();
        //std::cout<<"indice";
        std::cout<<report->gethtmlPath();

        delete report;
}
void MainWindow::on_connection_update_DUT(bool connection_status)
{
    // qDebug()<<"connection status="<<(int)connection_status;
    if (connection_status == true)
    {
        ui->label_9->setText("connected");
        ui->textBrowser_3->setText("ok");
        STATUS = true;
        qDebug() << STATUS;
    }
    else
    {
        ui->label_9->setText("not connected");
        ui->textBrowser_3->setText("not");
        STATUS = false;
        qDebug() << STATUS;
    }
}

void MainWindow::on_connection_update(bool connection_status)
{
    // qDebug()<<"connection status="<<(int)connection_status;
    if (connection_status == true)
    {   qDebug()<<"device connected to tester\n";
        ui->label_8->setText("connected");
         ui->label_9->setText("connected");
       // ui->textBrowser_3->setText("ok");
       //emit update_tester_stat(connection_status);
         ui->label_8->setStyleSheet("QLabel {  color : green; }");
         ui->label_9->setStyleSheet("QLabel {  color : green; }");
        STATUS = true;
        qDebug() << STATUS;
    }
    else
    {   qDebug()<<"device not connected to tester\n";
        ui->label_8->setText("not connected");
        ui->label_9->setText("not connected");
       // ui->textBrowser_3->setText("not");
       // emit update_tester_stat(connection_status);
        ui->label_8->setStyleSheet("QLabel {  color : red; }");
        ui->label_9->setStyleSheet("QLabel {  color : red; }");
        ///// to do later : add message box on connection lost.
        /*QMessageBox Msgbox;
        QMessageBox::information(MainWindow,tr("Alert!"),tr("Lost connection to tester, please check the cable!"));*/
        STATUS = false;
        qDebug() << STATUS;
    }
}
///////////////////////parse_config///////////////
/// \brief MainWindow::MainWindow
/// \param parent
void MainWindow::parseDataEntry(const QString dataPath)
{
    QString Name, Description, Value, lastSession = "";
    // Load our XML file.
    filePath = QFileDialog::getOpenFileName(NULL, "chose", "C://");
    QFile *xmlFile;
    xmlFile = new QFile(filePath);

    if (!xmlFile->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::critical(this,
                              "Error Loading Patient Data File",
                              QString("Could not load the patient XML data file at:\r\n  %0").arg(dataPath),
                              QMessageBox::Ok);
        return;
    }

    // Create an XML reader.
    QXmlStreamReader *xmlReader;
    xmlReader = new QXmlStreamReader(xmlFile);

    // Parse each element of the XML until we reach the end.
    while (!xmlReader->atEnd() && !xmlReader->hasError())
    {
        // Read next element
        QXmlStreamReader::TokenType token = xmlReader->readNext();

        // If token is just StartDocument - go to next
        if (token == QXmlStreamReader::StartDocument)
        {
            continue;
        }

        // If token is StartElement - read it
        if (token == QXmlStreamReader::StartElement)
        {

            if (xmlReader->name().toString() == "Name")
            {
                Name = xmlReader->readElementText();
            }
            else if (xmlReader->name().toString() == "Description")
            {
                Description = xmlReader->readElementText();
            }
            else if (xmlReader->name().toString() == "Value")
            {
                Value = xmlReader->readElementText();
            }
            else if (xmlReader->name().toString() == "Date")
            {
                lastSession = xmlReader->readElementText();
            }
        }
    }

    if (xmlReader->hasError())
    {
        QMessageBox::critical(this,
                              "Error Parsing Patient Data File",
                              QString("Error reading the patient file at:\r\n  %0,\r\nError:  %1").arg(dataPath, xmlReader->errorString()),
                              QMessageBox::Ok);
        return;
    }

    // close reader and flush file
    xmlReader->clear();
    xmlFile->close();

    // Delete
    delete xmlFile;
    delete xmlReader;
    QTableWidget *wdg = new QTableWidget;
    wdg->setColumnCount(3);
    wdg->setHorizontalHeaderLabels(QStringList{"Name", "Description", "Value"});
    wdg->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    // Add a new row to the table, with the data we parsed.
    wdg->insertRow(wdg->rowCount());
    wdg->setItem(wdg->rowCount() - 1, 0, new QTableWidgetItem(Name));
    wdg->setItem(wdg->rowCount() - 1, 1, new QTableWidgetItem(Description));

    wdg->setItem(wdg->rowCount() - 1, 2, new QTableWidgetItem(Value));

    //wdg->setItem(wdg->rowCount() - 1,3,new QTableWidgetItem(lastSession));

    wdg->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::show_test_result(int Test_Res, QModelIndex index)
{

    if (Test_Res == 0)
    {
        xmlFile->model->setData(index.siblingAtColumn(2), tr("Test_Passed"), Qt::DisplayRole);
    }
    else if (Test_Res == 1)
    {
        xmlFile->model->setData(index.siblingAtColumn(2), tr("Test_Failed"), Qt::DisplayRole);
    }
}
void MainWindow::on_parseXml_clicked(){
    QString filename = QDate::currentDate().toString("'Report_'MM_dd_yyyy'.html'");

    //qDebug()<<filename;
    char cmd[50];
    strcpy(cmd,"C:\\Users\\uid1321\\Desktop\\rep\\frontend-com-module\\IHM-module\\doc\\TestReport.html");
    system(cmd);

}


uint16_t MainWindow::trig(QString test)
{
    if (test == "TCP_BASICS_01")
    {
        return 1;
    }

    if (test == "TCP_BASICS_02")
    {
        return 2;
    }

    if (test == "TCP_BASICS_03")
    {
        return 3;
    }

    if (test == "TCP_BASICS_04")
    {
        return 4;
    }

    if (test == "TCP_BASICS_05")
    {
        return 5;
    }

    if (test == "TCP_BASICS_06")
    {
        return 6;
    }

    if (test == "TCP_BASICS_07")
    {
        return 7;
    }

    if (test == "TCP_BASICS_08")
    {
        return 8;
    }

    if (test == "TCP_BASICS_09")
    {
        return 9;
    }

    if (test == "TCP_BASICS_10")
    {
        return 10;
    }

    if (test == "TCP_BASICS_11")
    {
        return 11;
    }

    if (test == "TCP_BASICS_12")
    {
        return 12;
    }

    if (test == "TCP_BASICS_13")
    {
        return 13;
    }

    if (test == "TCP_BASICS_14")
    {
        return 14;
    }

    if (test == "TCP_BASICS_15")
    {
        return 15;
    }

    if (test == "TCP_CHECKSUM_01")
    {
        return 16;
    }

    if (test == "TCP_CHECKSUM_02")
    {
        return 17;
    }

    if (test == "TCP_CHECKSUM_03")
    {
        return 18;
    }

    if (test == "TCP_CHECKSUM_04")
    {
        return 19;
    }

    if (test == "TCP_UNACCEPTABLE_01")
    {
        return 20;
    }

    if (test == "TCP_UNACCEPTABLE_02")
    {
        return 21;
    }

    if (test == "TCP_UNACCEPTABLE_03")
    {
        return 22;
    }

    if (test == "TCP_UNACCEPTABLE_04")
    {
        return 23;
    }

    if (test == "TCP_UNACCEPTABLE_05")
    {
        return 24;
    }

    if (test == "TCP_UNACCEPTABLE_06")
    {
        return 25;
    }

    if (test == "TCP_UNACCEPTABLE_07")
    {
        return 26;
    }

    if (test == "TCP_UNACCEPTABLE_08")
    {
        return 27;
    }

    if (test == "TCP_UNACCEPTABLE_09")
    {
        return 28;
    }

    if (test == "TCP_UNACCEPTABLE_10")
    {
        return 29;
    }

    if (test == "TCP_UNACCEPTABLE_11")
    {
        return 30;
    }

    if (test == "TCP_UNACCEPTABLE_12")
    {
        return 31;
    }

    if (test == "TCP_FLAGS_INVALID_01")
    {
        return 32;
    }

    if (test == "TCP_FLAGS_INVALID_02")
    {
        return 33;
    }

    if (test == "TCP_FLAGS_INVALID_03")
    {
        return 34;
    }

    if (test == "TCP_FLAGS_INVALID_04")
    {
        return 35;
    }

    if (test == "TCP_FLAGS_INVALID_05")
    {
        return 36;
    }

    if (test == "TCP_FLAGS_INVALID_06")
    {
        return 37;
    }

    if (test == "TCP_FLAGS_INVALID_07")
    {
        return 38;
    }

    if (test == "TCP_FLAGS_INVALID_08")
    {
        return 39;
    }

    if (test == "TCP_FLAGS_INVALID_09")
    {
        return 40;
    }

    if (test == "TCP_FLAGS_INVALID_10")
    {
        return 41;
    }

    if (test == "TCP_FLAGS_INVALID_11")
    {
        return 42;
    }

    if (test == "TCP_FLAGS_INVALID_12")
    {
        return 43;
    }

    if (test == "TCP_FLAGS_INVALID_13")
    {
        return 44;
    }

    if (test == "TCP_FLAGS_INVALID_14")
    {
        return 45;
    }

    if (test == "TCP_FLAGS_INVALID_15")
    {
        return 46;
    }

    if (test == "TCP_FLAGS_PROCESSING_01")
    {
        return 47;
    }

    if (test == "TCP_FLAGS_PROCESSING_02")
    {
        return 48;
    }

    if (test == "TCP_FLAGS_PROCESSING_03")
    {
        return 49;
    }

    if (test == "TCP_FLAGS_PROCESSING_04")
    {
        return 50;
    }

    if (test == "TCP_FLAGS_PROCESSING_05")
    {
        return 51;
    }

    if (test == "TCP_FLAGS_PROCESSING_06")
    {
        return 52;
    }

    if (test == "TCP_FLAGS_PROCESSING_07")
    {
        return 53;
    }

    if (test == "TCP_FLAGS_PROCESSING_08")
    {
        return 54;
    }

    if (test == "TCP_FLAGS_PROCESSING_09")
    {
        return 55;
    }

    if (test == "TCP_FLAGS_PROCESSING_10")
    {
        return 56;
    }

    if (test == "TCP_FLAGS_PROCESSING_11")
    {
        return 57;
    }

    if (test == "TCP_MSS_OPTIONS_01")
    {
        return 58;
    }

    if (test == "TCP_MSS_OPTIONS_02")
    {
        return 59;
    }

    if (test == "TCP_MSS_OPTIONS_03")
    {
        return 60;
    }

    if (test == "TCP_MSS_OPTIONS_05")
    {
        return 61;
    }

    if (test == "TCP_MSS_OPTIONS_06")
    {
        return 62;
    }

    if (test == "TCP_MSS_OPTIONS_09")
    {
        return 63;
    }

    if (test == "TCP_MSS_OPTIONS_10")
    {
        return 64;
    }

    if (test == "TCP_MSS_OPTIONS_11")
    {
        return 65;
    }

    if (test == "TCP_MSS_OPTIONS_12")
    {
        return 66;
    }

    if (test == "TCP_OUT_OF_ORDER_01")
    {
        return 67;
    }

    if (test == "TCP_OUT_OF_ORDER_02")
    {
        return 68;
    }

    if (test == "TCP_OUT_OF_ORDER_03")
    {
        return 69;
    }

    if (test == "TCP_OUT_OF_ORDER_05")
    {
        return 70;
    }

    if (test == "TCP_RETRANSMISSION_TO_03")
    {
        return 71;
    }

    if (test == "TCP_RETRANSMISSION_TO_04")
    {
        return 72;
    }

    if (test == "TCP_RETRANSMISSION_TO_05")
    {
        return 73;
    }

    if (test == "TCP_RETRANSMISSION_TO_06")
    {
        return 74;
    }

    if (test == "TCP_RETRANSMISSION_TO_08")
    {
        return 75;
    }

    if (test == "TCP_RETRANSMISSION_TO_09")
    {
        return 76;
    }

    if (test == "UDP_MessageFormat")
    {
        return 77;
    }

    if (test == "UDP_DatagramLength")
    {
        return 78;
    }

    if (test == "UDP_Padding")
    {
        return 79;
    }

    if (test == "UDP_FIELDS_01")
    {
        return 80;
    }

    if (test == "UDP_FIELDS_02")
    {
        return 81;
    }

    if (test == "UDP_FIELDS_03")
    {
        return 82;
    }

    if (test == "UDP_FIELDS_04")
    {
        return 83;
    }

    if (test == "UDP_FIELDS_05")
    {
        return 84;
    }

    if (test == "UDP_FIELDS_06")
    {
        return 85;
    }

    if (test == "UDP_FIELDS_07")
    {
        return 86;
    }

    if (test == "UDP_FIELDS_08")
    {
        return 87;
    }

    if (test == "UDP_FIELDS_09")
    {
        return 88;
    }

    if (test == "UDP_FIELDS_10")
    {
        return 89;
    }

    if (test == "UDP_FIELDS_12")
    {
        return 90;
    }

    if (test == "UDP_FIELDS_13")
    {
        return 91;
    }

    if (test == "UDP_FIELDS_14")
    {
        return 92;
    }

    if (test == "UDP_FIELDS_15")
    {
        return 93;
    }

    if (test == "UDP_FIELDS_16")
    {
        return 94;
    }

    if (test == "UDP_USER_INTERFACE_01")
    {
        return 95;
    }

    if (test == "UDP_USER_INTERFACE_02")
    {
        return 96;
    }

    if (test == "UDP_USER_INTERFACE_03")
    {
        return 97;
    }

    if (test == "UDP_USER_INTERFACE_04")
    {
        return 98;
    }

    if (test == "UDP_USER_INTERFACE_05")
    {
        return 99;
    }

    if (test == "UDP_USER_INTERFACE_06")
    {
        return 100;
    }

    if (test == "UDP_USER_INTERFACE_07")
    {
        return 101;
    }

    if (test == "UDP_USER_INTERFACE_08")
    {
        return 102;
    }

    if (test == "UDP_INTRODUCTION_01")
    {
        return 103;
    }

    if (test == "UDP_INTRODUCTION_02")
    {
        return 104;
    }

    if (test == "UDP_INTRODUCTION_03")
    {
        return 105;
    }

    if (test == "UDP_INVALID_ADDRESSES_01")
    {
        return 106;
    }

    if (test == "UDP_INVALID_ADDRESSES_02")
    {
        return 107;
    }

    if (test == "ARP_01_Packet_Generation")
    {
        return 108;
    }

    if (test == "ARP_02_Packet_Generation")
    {
        return 109;
    }

    if (test == "ARP_03_Packet_Generation")
    {
        return 110;
    }

    if (test == "ARP_04_Packet_Generation")
    {
        return 111;
    }

    if (test == "ARP_05_Packet_Generation")
    {
        return 112;
    }

    if (test == "ARP_06_Packet_Generation")
    {
        return 113;
    }

    if (test == "ARP_07_Packet_Generation")
    {
        return 114;
    }

    if (test == "ARP_08_Packet_Generation")
    {
        return 115;
    }

    if (test == "ARP_09_Packet_Generation")
    {
        return 116;
    }

    if (test == "ARP_10_Packet_Generation")
    {
        return 117;
    }

    if (test == "ARP_11_Packet_Generation")
    {
        return 118;
    }

    if (test == "ARP_12_Packet_Generation")
    {
        return 119;
    }

    if (test == "ARP_13_Packet_Generation")
    {
        return 120;
    }

    if (test == "ARP_14_Packet_Generation")
    {
        return 121;
    }

    if (test == "ARP_15_Packet_Generation")
    {
        return 122;
    }

    if (test == "ARP_19_Packet_Generation")
    {
        return 123;
    }

    if (test == "ARP_16_Packet_Reception")
    {
        return 124;
    }

    if (test == "ARP_17_Packet_Reception")
    {
        return 125;
    }

    if (test == "ARP_18_Packet_Reception")
    {
        return 126;
    }

    if (test == "ARP_19_Packet_Reception")
    {
        return 127;
    }

    if (test == "ARP_20_Packet_Reception")
    {
        return 128;
    }

    if (test == "ARP_21_Packet_Reception")
    {
        return 129;
    }

    if (test == "ARP_22_Packet_Reception")
    {
        return 130;
    }

    if (test == "ARP_26_Packet_Reception")
    {
        return 131;
    }

    if (test == "ARP_27_Packet_Reception")
    {
        return 132;
    }

    if (test == "ARP_28_Packet_Reception")
    {
        return 133;
    }

    if (test == "ARP_32_Packet_Reception")
    {
        return 134;
    }

    if (test == "ARP_33_Packet_Reception")
    {
        return 135;
    }

    if (test == "ARP_34_Packet_Reception")
    {
        return 136;
    }

    if (test == "ARP_35_Packet_Reception")
    {
        return 137;
    }

    if (test == "ARP_36_Packet_Reception")
    {
        return 138;
    }

    if (test == "ARP_37_Packet_Reception")
    {
        return 139;
    }

    if (test == "ARP_38_Packet_Reception")
    {
        return 140;
    }

    if (test == "ARP_39_Packet_Reception")
    {
        return 141;
    }

    if (test == "ARP_40_Packet_Reception")
    {
        return 142;
    }

    if (test == "ARP_41_Packet_Reception")
    {
        return 143;
    }

    if (test == "ARP_42_Packet_Reception")
    {
        return 144;
    }

    if (test == "ARP_43_Packet_Reception")
    {
        return 145;
    }

    if (test == "ARP_44_Packet_Reception")
    {
        return 146;
    }

    if (test == "ARP_45_Packet_Reception")
    {
        return 147;
    }

    if (test == "ARP_46_Packet_Reception")
    {
        return 148;
    }

    if (test == "ARP_47_Packet_Reception")
    {
        return 149;
    }

    if (test == "ARP_48_Packet_Reception")
    {
        return 150;
    }

    if (test == "ARP_49_Packet_Reception")
    {
        return 151;
    }

    if (test == "ICMPv4_ERROR_02")
    {
        return 152;
    }

    if (test == "ICMPv4_ERROR_03")
    {
        return 153;
    }

    if (test == "ICMPv4_ERROR_04")
    {
        return 154;
    }

    if (test == "ICMPv4_ERROR_05")
    {
        return 155;
    }

    if (test == "ICMPv4_TYPE_04")
    {
        return 156;
    }

    if (test == "ICMPv4_TYPE_05")
    {
        return 157;
    }

    if (test == "ICMPv4_TYPE_08")
    {
        return 158;
    }

    if (test == "ICMPv4_TYPE_09")
    {
        return 159;
    }

    if (test == "ICMPv4_TYPE_10")
    {
        return 160;
    }

    if (test == "ICMPv4_TYPE_11")
    {
        return 161;
    }

    if (test == "ICMPv4_TYPE_12")
    {
        return 162;
    }

    if (test == "ICMPv4_TYPE_16")
    {
        return 163;
    }

    if (test == "ICMPv4_TYPE_18")
    {
        return 164;
    }

    if (test == "ICMPv4_TYPE_22")
    {
        return 165;
    }

    if (test == "IPv4_HEADER_01")
    {
        return 166;
    }

    if (test == "IPv4_HEADER_02")
    {
        return 167;
    }

    if (test == "IPv4_HEADER_03")
    {
        return 168;
    }

    if (test == "IPv4_HEADER_04")
    {
        return 169;
    }

    if (test == "IPv4_HEADER_05")
    {
        return 170;
    }

    if (test == "IPv4_HEADER_08")
    {
        return 171;
    }

    if (test == "IPv4_HEADER_09")
    {
        return 172;
    }

    if (test == "IPv4_CHECKSUM_02")
    {
        return 173;
    }

    if (test == "IPv4_CHECKSUM_05")
    {
        return 174;
    }

    if (test == "IPv4_TTL_01")
    {
        return 175;
    }

    if (test == "IPv4_TTL_05")
    {
        return 176;
    }

    if (test == "IPv4_VERSION_01")
    {
        return 177;
    }

    if (test == "IPv4_VERSION_03")
    {
        return 178;
    }

    if (test == "IPv4_VERSION_04")
    {
        return 179;
    }

    if (test == "IPv4_ADDRESSING_01")
    {
        return 180;
    }

    if (test == "IPv4_ADDRESSING_02")
    {
        return 181;
    }

    if (test == "IPv4_ADDRESSING_03")
    {
        return 182;
    }

    if (test == "IPv4_FRAGMENTS_01")
    {
        return 183;
    }

    if (test == "IPv4_FRAGMENTS_02")
    {
        return 184;
    }

    if (test == "IPv4_FRAGMENTS_03")
    {
        return 185;
    }

    if (test == "IPv4_FRAGMENTS_04")
    {
        return 186;
    }

    if (test == "IPv4_FRAGMENTS_05")
    {
        return 187;
    }

    if (test == "IPv4_REASSEMBLY_04")
    {
        return 188;
    }

    if (test == "IPv4_REASSEMBLY_06")
    {
        return 189;
    }

    if (test == "IPv4_REASSEMBLY_07")
    {
        return 190;
    }

    if (test == "IPv4_REASSEMBLY_09")
    {
        return 191;
    }

    if (test == "IPv4_REASSEMBLY_10")
    {
        return 192;
    }

    if (test == "IPv4_REASSEMBLY_11")
    {
        return 193;
    }

    if (test == "IPv4_REASSEMBLY_12")
    {
        return 194;
    }

    if (test == "IPv4_REASSEMBLY_13")
    {
        return 195;
    }

    if (test == "IPv4_AUTOCONF_INTRO")
    {
        return 196;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_01")
    {
        return 197;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_03")
    {
        return 198;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_05")
    {
        return 199;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_06")
    {
        return 200;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_07")
    {
        return 201;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_08")
    {
        return 202;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_09")
    {
        return 203;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_10")
    {
        return 204;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_11")
    {
        return 205;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_12")
    {
        return 206;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_13")
    {
        return 207;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_14")
    {
        return 208;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_15")
    {
        return 209;
    }

    if (test == "IPv4_AUTOCONF_ADDRESS_SELECTION_16")
    {
        return 210;
    }

    if (test == "IPv4_AUTOCONF_ANNOUNCING_01")
    {
        return 211;
    }

    if (test == "IPv4_AUTOCONF_ANNOUNCING_02")
    {
        return 212;
    }

    if (test == "IPv4_AUTOCONF_ANNOUNCING_03")
    {
        return 213;
    }

    if (test == "IPv4_AUTOCONF_ANNOUNCING_04")
    {
        return 214;
    }

    if (test == "IPv4_AUTOCONF_ANNOUNCING_05")
    {
        return 215;
    }

    if (test == "IPv4_AUTOCONF_ANNOUNCING_06")
    {
        return 216;
    }

    if (test == "IPv4_AUTOCONF_CONFLICT_06")
    {
        return 217;
    }

    if (test == "IPv4_AUTOCONF_CONFLICT_07")
    {
        return 218;
    }

    if (test == "IPv4_AUTOCONF_CONFLICT_08")
    {
        return 219;
    }

    if (test == "IPv4_AUTOCONF_CONFLICT_09")
    {
        return 220;
    }

    if (test == "IPv4_AUTOCONF_CONFLICT_10")
    {
        return 221;
    }

    if (test == "IPv4_AUTOCONF_CONFLICT_11")
    {
        return 222;
    }

    if (test == "IPv4_AUTOCONF_LINKLOCAL_PACKETS")
    {
        return 223;
    }

    if (test == "IPv4_AUTOCONF_NETWORK_PARTITIONS")
    {
        return 224;
    }

    if (test == "DHCPv4_CLIENT_SUMMARY_01")
    {
        return 225;
    }

    if (test == "DHCPv4_CLIENT_SUMMARY_02")
    {
        return 226;
    }

    if (test == "DHCPv4_CLIENT_SUMMARY_03")
    {
        return 227;
    }

    if (test == "DHCPv4_CLIENT_PROTOCOL_01")
    {
        return 228;
    }

    if (test == "DHCPv4_CLIENT_PROTOCOL_02")
    {
        return 229;
    }

    if (test == "DHCPv4_CLIENT_PROTOCOL_03")
    {
        return 230;
    }

    if (test == "DHCPv4_CLIENT_ALLOCATING_01")
    {
        return 231;
    }

    if (test == "DHCPv4_CLIENT_ALLOCATING_03")
    {
        return 232;
    }

    if (test == "DHCPv4_CLIENT_ALLOCATING_04")
    {
        return 233;
    }

    if (test == "DHCPv4_CLIENT_ALLOCATING_05")
    {
        return 234;
    }

    if (test == "DHCPv4_CLIENT_ALLOCATING_06")
    {
        return 235;
    }

    if (test == "DHCPv4_CLIENT_ALLOCATING_07")
    {
        return 236;
    }

    if (test == "DHCPv4_CLIENT_ALLOCATING_08")
    {
        return 237;
    }

    if (test == "DHCPv4_CLIENT_ALLOCATING_09")
    {
        return 238;
    }

    if (test == "DHCPv4_CLIENT_ALLOCATING_10")
    {
        return 239;
    }

    if (test == "DHCPv4_CLIENT_PARAMETERS_03")
    {
        return 240;
    }

    if (test == "DHCPv4_CLIENT_PARAMETERS_04")
    {
        return 241;
    }

    if (test == "DHCPv4_CLIENT_USAGE")
    {
        return 242;
    }

    if (test == "DHCPv4_CLIENT_CONSTRUCTING_MESSAGES_01")
    {
        return 243;
    }

    if (test == "DHCPv4_CLIENT_CONSTRUCTING_MESSAGES_02")
    {
        return 244;
    }

    if (test == "DHCPv4_CLIENT_CONSTRUCTING_MESSAGES_03")
    {
        return 245;
    }

    if (test == "DHCPv4_CLIENT_CONSTRUCTING_MESSAGES_04")
    {
        return 246;
    }

    if (test == "DHCPv4_CLIENT_CONSTRUCTING_MESSAGES_05")
    {
        return 247;
    }

    if (test == "DHCPv4_CLIENT_CONSTRUCTING_MESSAGES_06")
    {
        return 248;
    }

    if (test == "DHCPv4_CLIENT_CONSTRUCTING_MESSAGES_12")
    {
        return 249;
    }

    if (test == "DHCPv4_CLIENT_CONSTRUCTING_MESSAGES_13")
    {
        return 250;
    }

    if (test == "DHCPv4_CLIENT_CONSTRUCTING_MESSAGES_14")
    {
        return 251;
    }

    if (test == "DHCPv4_CLIENT_REQUEST_01")
    {
        return 252;
    }

    if (test == "DHCPv4_CLIENT_REQUEST_02")
    {
        return 253;
    }

    if (test == "DHCPv4_CLIENT_REQUEST_06")
    {
        return 254;
    }

    if (test == "DHCPv4_CLIENT_REQUEST_07")
    {
        return 255;
    }

    if (test == "DHCPv4_CLIENT_REQUEST_08")
    {
        return 256;
    }

    if (test == "DHCPv4_CLIENT_REQUEST_09")
    {
        return 257;
    }

    if (test == "DHCPv4_CLIENT_REQUEST_10")
    {
        return 258;
    }

    if (test == "DHCPv4_CLIENT_REQUEST_11")
    {
        return 259;
    }

    if (test == "DHCPv4_CLIENT_REQUEST_12")
    {
        return 260;
    }

    if (test == "DHCPv4_CLIENT_INITIALIZATION_ALLOCATION_01")
    {
        return 261;
    }

    if (test == "DHCPv4_CLIENT_INITIALIZATION_ALLOCATION_02")
    {
        return 262;
    }

    if (test == "DHCPv4_CLIENT_INITIALIZATION_ALLOCATION_03")
    {
        return 263;
    }

    if (test == "DHCPv4_CLIENT_INITIALIZATION_ALLOCATION_04")
    {
        return 264;
    }

    if (test == "DHCPv4_CLIENT_INITIALIZATION_ALLOCATION_05")
    {
        return 265;
    }

    if (test == "DHCPv4_CLIENT_INITIALIZATION_ALLOCATION_06")
    {
        return 266;
    }

    if (test == "DHCPv4_CLIENT_INITIALIZATION_ALLOCATION_08")
    {
        return 267;
    }

    if (test == "DHCPv4_CLIENT_INITIALIZATION_ALLOCATION_09")
    {
        return 268;
    }

    if (test == "DHCPv4_CLIENT_INITIALIZATION_ALLOCATION_10")
    {
        return 269;
    }

    if (test == "DHCPv4_CLIENT_REACQUISITION_01")
    {
        return 270;
    }

    if (test == "DHCPv4_CLIENT_REACQUISITION_02")
    {
        return 271;
    }

    if (test == "DHCPv4_CLIENT_REACQUISITION_03")
    {
        return 272;
    }

    if (test == "DHCPv4_CLIENT_REACQUISITION_04")
    {
        return 273;
    }

    if (test == "DHCPv4_CLIENT_REACQUISITION_05")
    {
        return 274;
    }

    if (test == "DHCPv4_CLIENT_REACQUISITION_06")
    {
        return 275;
    }

    if (test == "DHCPv4_CLIENT_REACQUISITION_07")
    {
        return 276;
    }

    if (test == "DHCPv4_CLIENT_REACQUISITION_08")
    {
        return 277;
    }

    if (test == "SOMEIPSRV_FORMAT_01")
    {
        return 278;
    }

    if (test == "SOMEIPSRV_FORMAT_02")
    {
        return 279;
    }

    if (test == "SOMEIPSRV_FORMAT_03")
    {
        return 280;
    }

    if (test == "SOMEIPSRV_FORMAT_04")
    {
        return 281;
    }

    if (test == "SOMEIPSRV_FORMAT_05")
    {
        return 282;
    }

    if (test == "SOMEIPSRV_FORMAT_06")
    {
        return 283;
    }

    if (test == "SOMEIPSRV_FORMAT_07")
    {
        return 284;
    }

    if (test == "SOMEIPSRV_FORMAT_08")
    {
        return 285;
    }

    if (test == "SOMEIPSRV_FORMAT_09")
    {
        return 286;
    }

    if (test == "SOMEIPSRV_FORMAT_10")
    {
        return 287;
    }

    if (test == "SOMEIPSRV_FORMAT_11")
    {
        return 288;
    }

    if (test == "SOMEIPSRV_FORMAT_12")
    {
        return 289;
    }

    if (test == "SOMEIPSRV_FORMAT_13")
    {
        return 290;
    }

    if (test == "SOMEIPSRV_FORMAT_14")
    {
        return 291;
    }

    if (test == "SOMEIPSRV_FORMAT_15")
    {
        return 292;
    }

    if (test == "SOMEIPSRV_FORMAT_16")
    {
        return 293;
    }

    if (test == "SOMEIPSRV_FORMAT_17")
    {
        return 294;
    }

    if (test == "SOMEIPSRV_FORMAT_18")
    {
        return 295;
    }

    if (test == "SOMEIPSRV_FORMAT_19")
    {
        return 296;
    }

    if (test == "SOMEIPSRV_FORMAT_20")
    {
        return 297;
    }

    if (test == "SOMEIPSRV_FORMAT_21")
    {
        return 298;
    }

    if (test == "SOMEIPSRV_FORMAT_23")
    {
        return 299;
    }

    if (test == "SOMEIPSRV_FORMAT_24")
    {
        return 300;
    }

    if (test == "SOMEIPSRV_FORMAT_25")
    {
        return 301;
    }

    if (test == "SOMEIPSRV_FORMAT_26")
    {
        return 302;
    }

    if (test == "SOMEIPSRV_FORMAT_27")
    {
        return 303;
    }

    if (test == "SOMEIPSRV_FORMAT_28")
    {
        return 304;
    }

    if (test == "SOMEIPSRV_OPTIONS_01")
    {
        return 305;
    }

    if (test == "SOMEIPSRV_OPTIONS_02")
    {
        return 306;
    }

    if (test == "SOMEIPSRV_OPTIONS_03")
    {
        return 307;
    }

    if (test == "SOMEIPSRV_OPTIONS_04")
    {
        return 308;
    }

    if (test == "SOMEIPSRV_OPTIONS_05")
    {
        return 309;
    }

    if (test == "SOMEIPSRV_OPTIONS_06")
    {
        return 310;
    }

    if (test == "SOMEIPSRV_OPTIONS_07")
    {
        return 311;
    }

    if (test == "SOMEIPSRV_OPTIONS_08")
    {
        return 312;
    }

    if (test == "SOMEIPSRV_OPTIONS_09")
    {
        return 313;
    }

    if (test == "SOMEIPSRV_OPTIONS_10")
    {
        return 314;
    }

    if (test == "SOMEIPSRV_OPTIONS_11")
    {
        return 315;
    }

    if (test == "SOMEIPSRV_OPTIONS_12")
    {
        return 316;
    }

    if (test == "SOMEIPSRV_OPTIONS_13")
    {
        return 317;
    }

    if (test == "SOMEIPSRV_OPTIONS_14")
    {
        return 318;
    }

    if (test == "SOMEIPSRV_OPTIONS_15")
    {
        return 319;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_01")
    {
        return 320;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_02")
    {
        return 321;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_03")
    {
        return 322;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_04")
    {
        return 323;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_05")
    {
        return 324;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_06")
    {
        return 325;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_07")
    {
        return 326;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_08")
    {
        return 327;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_09")
    {
        return 328;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_11")
    {
        return 329;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_13")
    {
        return 330;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_14")
    {
        return 331;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_15")
    {
        return 332;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_16")
    {
        return 333;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_17")
    {
        return 334;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_18")
    {
        return 335;
    }

    if (test == "SOMEIPSRV_SD_MESSAGE_19")
    {
        return 336;
    }

    if (test == "SOMEIPSRV_SD_BEHAVIOR_01")
    {
        return 337;
    }

    if (test == "SOMEIPSRV_SD_BEHAVIOR_02")
    {
        return 338;
    }

    if (test == "SOMEIPSRV_SD_BEHAVIOR_03")
    {
        return 339;
    }

    if (test == "SOMEIPSRV_SD_BEHAVIOR_04")
    {
        return 340;
    }

    if (test == "SOMEIPSRV_BASIC_01")
    {
        return 341;
    }

    if (test == "SOMEIPSRV_BASIC_02")
    {
        return 342;
    }

    if (test == "SOMEIPSRV_BASIC_03")
    {
        return 343;
    }

    if (test == "SOMEIPSRV_ONWIRE_01")
    {
        return 344;
    }

    if (test == "SOMEIPSRV_ONWIRE_02")
    {
        return 345;
    }

    if (test == "SOMEIPSRV_ONWIRE_03")
    {
        return 346;
    }

    if (test == "SOMEIPSRV_ONWIRE_04")
    {
        return 347;
    }

    if (test == "SOMEIPSRV_ONWIRE_05")
    {
        return 348;
    }

    if (test == "SOMEIPSRV_ONWIRE_06")
    {
        return 349;
    }

    if (test == "SOMEIPSRV_ONWIRE_07")
    {
        return 350;
    }

    if (test == "SOMEIPSRV_ONWIRE_10")
    {
        return 351;
    }

    if (test == "SOMEIPSRV_ONWIRE_11")
    {
        return 352;
    }

    if (test == "SOMEIPSRV_ONWIRE_12")
    {
        return 353;
    }

    if (test == "SOMEIPSRV_RPC_01")
    {
        return 354;
    }

    if (test == "SOMEIPSRV_RPC_02")
    {
        return 355;
    }

    if (test == "SOMEIPSRV_RPC_03")
    {
        return 356;
    }

    if (test == "SOMEIPSRV_RPC_04")
    {
        return 357;
    }

    if (test == "SOMEIPSRV_RPC_05")
    {
        return 358;
    }

    if (test == "SOMEIPSRV_RPC_06")
    {
        return 359;
    }

    if (test == "SOMEIPSRV_RPC_07")
    {
        return 360;
    }

    if (test == "SOMEIPSRV_RPC_08")
    {
        return 361;
    }

    if (test == "SOMEIPSRV_RPC_09")
    {
        return 362;
    }

    if (test == "SOMEIPSRV_RPC_10")
    {
        return 363;
    }

    if (test == "SOMEIPSRV_RPC_11")
    {
        return 364;
    }

    if (test == "SOMEIPSRV_RPC_13")
    {
        return 365;
    }

    if (test == "SOMEIPSRV_RPC_14")
    {
        return 366;
    }

    if (test == "SOMEIPSRV_RPC_17")
    {
        return 367;
    }

    if (test == "SOMEIPSRV_RPC_18")
    {
        return 368;
    }

    if (test == "SOMEIPSRV_RPC_19")
    {
        return 369;
    }

    if (test == "SOMEIPSRV_RPC_20")
    {
        return 370;
    }

    if (test == "SOMEIP_ETS_001")
    {
        return 371;
    }

    if (test == "SOMEIP_ETS_002")
    {
        return 372;
    }

    if (test == "SOMEIP_ETS_003")
    {
        return 373;
    }

    if (test == "SOMEIP_ETS_004")
    {
        return 374;
    }

    if (test == "SOMEIP_ETS_005")
    {
        return 375;
    }

    if (test == "SOMEIP_ETS_007")
    {
        return 376;
    }

    if (test == "SOMEIP_ETS_008")
    {
        return 377;
    }

    if (test == "SOMEIP_ETS_009")
    {
        return 378;
    }

    if (test == "SOMEIP_ETS_019")
    {
        return 379;
    }

    if (test == "SOMEIP_ETS_021")
    {
        return 380;
    }

    if (test == "SOMEIP_ETS_022")
    {
        return 381;
    }

    if (test == "SOMEIP_ETS_027")
    {
        return 382;
    }

    if (test == "SOMEIP_ETS_028")
    {
        return 383;
    }

    if (test == "SOMEIP_ETS_029")
    {
        return 384;
    }

    if (test == "SOMEIP_ETS_030")
    {
        return 385;
    }

    if (test == "SOMEIP_ETS_031")
    {
        return 386;
    }

    if (test == "SOMEIP_ETS_032")
    {
        return 387;
    }

    if (test == "SOMEIP_ETS_033")
    {
        return 388;
    }

    if (test == "SOMEIP_ETS_034")
    {
        return 389;
    }

    if (test == "SOMEIP_ETS_035")
    {
        return 390;
    }

    if (test == "SOMEIP_ETS_037")
    {
        return 391;
    }

    if (test == "SOMEIP_ETS_038")
    {
        return 392;
    }

    if (test == "SOMEIP_ETS_039")
    {
        return 393;
    }

    if (test == "SOMEIP_ETS_040")
    {
        return 394;
    }

    if (test == "SOMEIP_ETS_041")
    {
        return 395;
    }

    if (test == "SOMEIP_ETS_042")
    {
        return 396;
    }

    if (test == "SOMEIP_ETS_043")
    {
        return 397;
    }

    if (test == "SOMEIP_ETS_044")
    {
        return 398;
    }

    if (test == "SOMEIP_ETS_045")
    {
        return 399;
    }

    if (test == "SOMEIP_ETS_046")
    {
        return 400;
    }

    if (test == "SOMEIP_ETS_047")
    {
        return 401;
    }

    if (test == "SOMEIP_ETS_048")
    {
        return 402;
    }

    if (test == "SOMEIP_ETS_049")
    {
        return 403;
    }

    if (test == "SOMEIP_ETS_050")
    {
        return 404;
    }

    if (test == "SOMEIP_ETS_051")
    {
        return 405;
    }

    if (test == "SOMEIP_ETS_052")
    {
        return 406;
    }

    if (test == "SOMEIP_ETS_053")
    {
        return 407;
    }

    if (test == "SOMEIP_ETS_054")
    {
        return 408;
    }

    if (test == "SOMEIP_ETS_055")
    {
        return 409;
    }

    if (test == "SOMEIP_ETS_058")
    {
        return 410;
    }

    if (test == "SOMEIP_ETS_059")
    {
        return 411;
    }

    if (test == "SOMEIP_ETS_060")
    {
        return 412;
    }

    if (test == "SOMEIP_ETS_061")
    {
        return 413;
    }

    if (test == "SOMEIP_ETS_063")
    {
        return 414;
    }

    if (test == "SOMEIP_ETS_064")
    {
        return 415;
    }

    if (test == "SOMEIP_ETS_065")
    {
        return 416;
    }

    if (test == "SOMEIP_ETS_066")
    {
        return 417;
    }

    if (test == "SOMEIP_ETS_067")
    {
        return 418;
    }

    if (test == "SOMEIP_ETS_068")
    {
        return 419;
    }

    if (test == "SOMEIP_ETS_069")
    {
        return 420;
    }

    if (test == "SOMEIP_ETS_070")
    {
        return 421;
    }

    if (test == "SOMEIP_ETS_071")
    {
        return 422;
    }

    if (test == "SOMEIP_ETS_072")
    {
        return 423;
    }

    if (test == "SOMEIP_ETS_073")
    {
        return 424;
    }

    if (test == "SOMEIP_ETS_074")
    {
        return 425;
    }

    if (test == "SOMEIP_ETS_075")
    {
        return 426;
    }

    if (test == "SOMEIP_ETS_076")
    {
        return 427;
    }

    if (test == "SOMEIP_ETS_077")
    {
        return 428;
    }

    if (test == "SOMEIP_ETS_078")
    {
        return 429;
    }

    if (test == "SOMEIP_ETS_081")
    {
        return 430;
    }

    if (test == "SOMEIP_ETS_082")
    {
        return 431;
    }

    if (test == "SOMEIP_ETS_084")
    {
        return 432;
    }

    if (test == "SOMEIP_ETS_085")
    {
        return 433;
    }

    if (test == "SOMEIP_ETS_086")
    {
        return 434;
    }

    if (test == "SOMEIP_ETS_087")
    {
        return 435;
    }

    if (test == "SOMEIP_ETS_088")
    {
        return 436;
    }

    if (test == "SOMEIP_ETS_089")
    {
        return 437;
    }

    if (test == "SOMEIP_ETS_091")
    {
        return 438;
    }

    if (test == "SOMEIP_ETS_092")
    {
        return 439;
    }

    if (test == "SOMEIP_ETS_093")
    {
        return 440;
    }

    if (test == "SOMEIP_ETS_094")
    {
        return 441;
    }

    if (test == "SOMEIP_ETS_095")
    {
        return 442;
    }

    if (test == "SOMEIP_ETS_096")
    {
        return 443;
    }

    if (test == "SOMEIP_ETS_097")
    {
        return 444;
    }

    if (test == "SOMEIP_ETS_098")
    {
        return 445;
    }

    if (test == "SOMEIP_ETS_099")
    {
        return 446;
    }

    if (test == "SOMEIP_ETS_100")
    {
        return 447;
    }

    if (test == "SOMEIP_ETS_101")
    {
        return 448;
    }

    if (test == "SOMEIP_ETS_103")
    {
        return 449;
    }

    if (test == "SOMEIP_ETS_104")
    {
        return 450;
    }

    if (test == "SOMEIP_ETS_105")
    {
        return 451;
    }

    if (test == "SOMEIP_ETS_106")
    {
        return 452;
    }

    if (test == "SOMEIP_ETS_107")
    {
        return 453;
    }

    if (test == "SOMEIP_ETS_108")
    {
        return 454;
    }

    if (test == "SOMEIP_ETS_109")
    {
        return 455;
    }

    if (test == "SOMEIP_ETS_110")
    {
        return 456;
    }

    if (test == "SOMEIP_ETS_111")
    {
        return 457;
    }

    if (test == "SOMEIP_ETS_112")
    {
        return 458;
    }

    if (test == "SOMEIP_ETS_113")
    {
        return 459;
    }

    if (test == "SOMEIP_ETS_114")
    {
        return 460;
    }

    if (test == "SOMEIP_ETS_115")
    {
        return 461;
    }

    if (test == "SOMEIP_ETS_116")
    {
        return 462;
    }

    if (test == "SOMEIP_ETS_117")
    {
        return 463;
    }

    if (test == "SOMEIP_ETS_118")
    {
        return 464;
    }

    if (test == "SOMEIP_ETS_119")
    {
        return 465;
    }

    if (test == "SOMEIP_ETS_120")
    {
        return 466;
    }

    if (test == "SOMEIP_ETS_121")
    {
        return 467;
    }

    if (test == "SOMEIP_ETS_122")
    {
        return 468;
    }

    if (test == "SOMEIP_ETS_123")
    {
        return 469;
    }

    if (test == "SOMEIP_ETS_124")
    {
        return 470;
    }

    if (test == "SOMEIP_ETS_125")
    {
        return 471;
    }

    if (test == "SOMEIP_ETS_127")
    {
        return 472;
    }

    if (test == "SOMEIP_ETS_128")
    {
        return 473;
    }

    if (test == "SOMEIP_ETS_130")
    {
        return 474;
    }

    if (test == "SOMEIP_ETS_134")
    {
        return 475;
    }

    if (test == "SOMEIP_ETS_135")
    {
        return 476;
    }

    if (test == "SOMEIP_ETS_136")
    {
        return 477;
    }

    if (test == "SOMEIP_ETS_137")
    {
        return 478;
    }

    if (test == "SOMEIP_ETS_138")
    {
        return 479;
    }

    if (test == "SOMEIP_ETS_139")
    {
        return 480;
    }

    if (test == "SOMEIP_ETS_140")
    {
        return 481;
    }

    if (test == "SOMEIP_ETS_141")
    {
        return 482;
    }

    if (test == "SOMEIP_ETS_142")
    {
        return 483;
    }

    if (test == "SOMEIP_ETS_143")
    {
        return 484;
    }

    if (test == "SOMEIP_ETS_144")
    {
        return 485;
    }

    if (test == "SOMEIP_ETS_146")
    {
        return 486;
    }

    if (test == "SOMEIP_ETS_147")
    {
        return 487;
    }

    if (test == "SOMEIP_ETS_148")
    {
        return 488;
    }

    if (test == "SOMEIP_ETS_149")
    {
        return 489;
    }

    if (test == "SOMEIP_ETS_150")
    {
        return 490;
    }

    if (test == "SOMEIP_ETS_151")
    {
        return 491;
    }

    if (test == "SOMEIP_ETS_152")
    {
        return 492;
    }

    if (test == "SOMEIP_ETS_153")
    {
        return 493;
    }

    if (test == "SOMEIP_ETS_154")
    {
        return 494;
    }

    if (test == "SOMEIP_ETS_155")
    {
        return 495;
    }

    return 0;
}

void MainWindow::handleResults(const QString &){}
void MainWindow::test_runner_routine()
{
    emit signalSendButtonEnable(true);   //send signal

}
void MainWindow::singalReceiveButtonEnable(int t){
qDebug()<<"Update progress bar to value="<<t<<"\n";
try {
    ui->progressBar->setValue(t) ;
} catch (...) {
}

}
void MainWindow::Reset(){
    QModelIndex index;

    index = ui->treeView->indexAt(ui->treeView->rect().topLeft());
    int counter =1;
    while (index.isValid())
    {

    }

}
void MainWindow::on_actionRun_triggered()
{
    /* Check if the application is connected to the tes device */
    if(connection_status == false)
    {
        QMessageBox Msgbox;
        QMessageBox::information(this,tr("Alert!"),tr("Please connect to the Tester device first!"));
         return;
    }


    test_list_to_run.clear();
    int test_result = 1;
    test_info test_informations;

    QModelIndex index;

    index = ui->treeView->indexAt(ui->treeView->rect().topLeft());
    int counter =153;
    while (index.isValid())
    {

        if (ui->treeView->model()->data(index, Qt::CheckStateRole) == Qt::Checked)
        {
            QString test_case = ui->treeView->model()->data(index).toString();
            uint16_t t_id = trig(test_case);
            test_informations.test_id = (int)t_id;
            test_informations.index = index;
            test_list_to_run.push_back(test_informations);
            //qDebug() << "++++++++++++++++++++++\n";
            qDebug()<< "if (test == "<<test_case <<")\n {\n return "<<counter <<"; }\n";
            counter++;   
        }
        else
        {
            //qDebug() << "OK";
        }
        index = ui->treeView->indexBelow(index);
    }
    connect(this, SIGNAL(signalSendButtonEnable(int)), this, SLOT(singalReceiveButtonEnable(int)));

    qDebug() << "number of tests to run== " << test_list_to_run.size();
    std::thread t([=](){
        int counter= 0;
        qDebug() << "Hello from the other thread\n";
        int total_number_of_tests= test_list_to_run.size();
        int progress_bar_step = 100/total_number_of_tests;
        int progress_bar_value = 0;
        for (auto it = test_list_to_run.begin(); it != test_list_to_run.end(); it++)
        {
            progress_bar_value=progress_bar_value+progress_bar_step;
            emit signalSendButtonEnable(progress_bar_value);   //send signal


            uint16_t _test_id = (uint16_t)it->test_id;
            uint16_t test_result = ComModuleInstance.StartTest(_test_id);
             //uint16_t test_result = 0;
             //Sleep(1000);
            /* for simulation needs
             * Sleep(2000);
            if(counter++ >2)test_result=5;*/
            QModelIndex index = it->index;
            if (test_result == 0)
            {
                xmlFile->model->setData(index.siblingAtColumn(2), tr("Test Passed"), Qt::DisplayRole);
                time_t rawtime;
                  struct tm * timeinfo;
                  char buffer[80];

                  time (&rawtime);
                  timeinfo = localtime(&rawtime);

                  strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
                  std::string str(buffer);
                testDataCreator.updateTestStatus((int)_test_id,PASSED,true,str,"Test Passed OK");
                //inttestId,result, testResult,bool resultIsReady, std::string timestamp, std::string testResultCause
                xmlFile->model->itemFromIndex(index.siblingAtColumn(0))->setBackground(Qt::green);
                xmlFile->model->itemFromIndex(index.siblingAtColumn(1))->setBackground(Qt::green);
                xmlFile->model->itemFromIndex(index.siblingAtColumn(2))->setBackground(Qt::green);
            }
            else if(test_result == 1)
            {
                time_t rawtime;
                  struct tm * timeinfo;
                  char buffer[80];

                  time (&rawtime);
                  timeinfo = localtime(&rawtime);
                  strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
                  std::string str(buffer);
                testDataCreator.updateTestStatus((int)_test_id,FAILED,true,str,"Test Failed");
                xmlFile->model->setData(index.siblingAtColumn(2), tr("Test Failed"), Qt::DisplayRole);
                xmlFile->model->itemFromIndex(index.siblingAtColumn(0))->setBackground(Qt::red);
                xmlFile->model->itemFromIndex(index.siblingAtColumn(1))->setBackground(Qt::red);
                xmlFile->model->itemFromIndex(index.siblingAtColumn(2))->setBackground(Qt::red);

            }
            else
            {
                time_t rawtime;
                  struct tm * timeinfo;
                  char buffer[80];

                  time (&rawtime);
                  timeinfo = localtime(&rawtime);
                  strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
                  std::string str(buffer);
                testDataCreator.updateTestStatus((int)_test_id,NOT_EXECUTED,true,str,"Test Unavailable");
                xmlFile->model->setData(index.siblingAtColumn(2), tr("Test Unavailable"), Qt::DisplayRole);
                xmlFile->model->itemFromIndex(index.siblingAtColumn(0))->setBackground(Qt::yellow);
                xmlFile->model->itemFromIndex(index.siblingAtColumn(1))->setBackground(Qt::yellow);
                xmlFile->model->itemFromIndex(index.siblingAtColumn(2))->setBackground(Qt::yellow);
            }
        }
        /* After completing all the tests, set the progress bar to 100 % */
        progress_bar_value = 100;
        emit signalSendButtonEnable(progress_bar_value);   //send signal



        generate_report();

        //QTextBrowser *tb = new QTextBrowser(ui->tabWidget_2);
        //tb->setOpenExternalLinks(true);


        });
        t.detach();


   // std::thread test_runner_thread(&MainWindow::test_runner_routine, this);
   // test_runner_thread.detach();test_runner_thread.
   // test_runner_thread.join();

    //test_runner_thread(&MainWindow::test_runner_routine, this);
    //test_runner_thread.detach(); // as opposed to .join, which runs on the current thread
}

void MainWindow::on_action_Open_triggered()
{
}
void MainWindow::on_actionSave_triggered()
{
    xmlFile->writeXml();
    /*QString filename = QDate::currentDate().toString("'C:\\Users\\uid1321\\Desktop\\rep\\frontend-com-module\\IHM-module\\doc\\Report_'yyyy_MM_dd'.html'");

    ui->textBrowser->setSource(QUrl(filename));*/
}


void MainWindow::on_pushButton_3_clicked()
{
    ComModuleInstance.DisConnect();
    ui->label_8->setText("DisConnected");
    ui->label_9->setText("DisConnected");
    ui->label_8->setStyleSheet("QLabel {  color : red; }");
    ui->label_9->setStyleSheet("QLabel {  color : red; }");
}
void MainWindow::setStatus(void *)
{

    if (STATUS == true)
    {   QColor color = QColorDialog::getColor(Qt::green, this);
        QPalette palette = ui->label_8->palette();
        palette.setColor(QPalette::WindowText, color);
        ui->label_8->setPalette(palette);
        ui->label_8->setText("connected");



        ui->label_9->setPalette(palette);
        ui->label_9->setText("connected");

    }
    else
    {
        ui->label_8->setText("notconnected");
    }
}

/*void MainWindow::RegisterTesterConnection( void (*on_connection_update)(bool status) )
{
//to be defined
on_connection_update(true);
_sleep(5);
on_connection_update(false);
_sleep(5);

}*/
void MainWindow::on_pushButton_4_clicked()
{
    //ComModuleInstance.RegisterTesterConnection_DUT(std::bind(&MainWindow::on_connection_update_DUT, this, _1));
}
void MainWindow::on_pushButton_2_clicked()
{

    std::string adress = ui->serveurIP->text().toStdString();
    int port1 = ui->serveurPort->text().toInt();
    std::string adress1 = adress;
    ComModuleInstance.ConnectToTester(adress1, port1);
    ComModuleInstance.RegisterTesterConnection(std::bind(&MainWindow::on_connection_update, this, _1));
    connection_status = true;

           /* ui->label_8->setText("Connected");
            ui->label_9->setText("Connected");
            ui->label_8->setStyleSheet("QLabel {  color : green; }");
            ui->label_9->setStyleSheet("QLabel {  color : green; }");*/

}

void MainWindow::on_pushButton_clicked()
{
    /*QTableWidget *wdg = new QTableWidget;
    wdg->setColumnCount(3);
    wdg->setRowCount(5);
    wdg->setHorizontalHeaderLabels(QStringList{"Name", "Description", "Value"});
    wdg->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    wdg->height();
    QTableWidgetItem *newItem = new QTableWidgetItem(tr("[IPVersion]").arg(
           (1)*(1)));
    wdg->setItem(0, 0, newItem);
    QTableWidgetItem *newItem1 = new QTableWidgetItem(tr("version of the IP protocol").arg(
           (1)*(1)));
    wdg->setItem(0,1,newItem1);

    QTableWidgetItem *newItem2 = new QTableWidgetItem(tr("ipv4").arg(
           (1)*(1)));
    wdg->setItem(0,2,newItem2);
    QTableWidgetItem *newItem3 = new QTableWidgetItem(tr("[localHostTCP]").arg(
           (1)*(1)));
    wdg->setItem(1, 0, newItem3);
    QTableWidgetItem *newItem4 = new QTableWidgetItem(tr("local adress of the HMI Host ").arg(
           (1)*(1)));
    wdg->setItem(1,1,newItem4);

    QTableWidgetItem *newItem5 = new QTableWidgetItem(tr("192.168.56.1").arg(
           (1)*(1)));
    wdg->setItem(1,2,newItem5);
    QTableWidgetItem *newItem6 = new QTableWidgetItem(tr("[localPort]").arg(
           (1)*(1)));
    wdg->setItem(2, 0, newItem6);
    QTableWidgetItem *newItem7 = new QTableWidgetItem(tr("local port of the HMI host").arg(
           (1)*(1)));
    wdg->setItem(2,1,newItem7);

    QTableWidgetItem *newItem8 = new QTableWidgetItem(tr("12345").arg(
           (1)*(1)));
    wdg->setItem(2,2,newItem8);
    QTableWidgetItem *newItem9 = new QTableWidgetItem(tr("[remoteHostTCP]").arg(
           (1)*(1)));
    wdg->setItem(3, 0, newItem9);
    QTableWidgetItem *newItem10 = new QTableWidgetItem(tr("remote adress of the DUT Host").arg(
           (1)*(1)));
    wdg->setItem(3,1,newItem10);

    QTableWidgetItem *newItem11 = new QTableWidgetItem(tr("192.168.56.1").arg(
           (1)*(1)));
    wdg->setItem(3,2,newItem11);
    QTableWidgetItem *newItem12 = new QTableWidgetItem(tr("[remotePortTCP]").arg(
           (1)*(1)));
    wdg->setItem(4, 0, newItem12);
    QTableWidgetItem *newItem13 = new QTableWidgetItem(tr("remote port of the DUT Host").arg(
           (1)*(1)));
    wdg->setItem(4,1,newItem13);

    QTableWidgetItem *newItem14 = new QTableWidgetItem(tr("12000").arg(
           (1)*(1)));
    wdg->setItem(4,2,newItem14);
    QTableWidgetItem *newItem15 = new QTableWidgetItem(tr("[iterations]").arg(
           (1)*(1)));
    wdg->setItem(5,0,newItem15);
    QTableWidgetItem *newItem16 = new QTableWidgetItem(tr("iterations number").arg(
           (1)*(1)));
    wdg->setItem(5,1,newItem16);
    QTableWidgetItem *newItem17 = new QTableWidgetItem(tr("1").arg(
           (1)*(1)));
    wdg->setItem(5,2,newItem17);

    wdg->show();*/
    //QString path = "C:\\Users\\uid1321\Downloads\qt_xml_parse-master\qt_xml_parse-master\build-QDomxmlParse-Desktop_Qt_6_0_2_MinGW_64_bit-Debug\debug\data\Ethernet";
    //parseDataEntry("C:\\Users\\uid1321\\Documents\\Ethernet");
    GlobalConfig Conf;
    strcpy(Conf.Dut_address,ui->serveurIP->text().toStdString().c_str());
    //Conf.Dut_address=ui->serveurIP->text().toStdString();
    Conf.Dut_Port= ui->serveurPort->text().toInt();
    ComModuleInstance.SetGlobalConfig(Conf);
}

void MainWindow::on_printInfo_clicked()
{ //xmlFile->startParse();
    QModelIndex index;
    index = ui->treeView->indexAt(ui->treeView->rect().topLeft());
    while (index.isValid())
    {
        if (ui->treeView->model()->data(index, Qt::CheckStateRole) == Qt::Checked)
        { //qDebug()<< ui->treeView->model()->data(index);

            qDebug() << index.siblingAtColumn(1).data().toString();

            qDebug() << "Selected";
            QString test = index.siblingAtColumn(1).data().toString();
            qDebug() <<"test name="<<test<<",Description="<<index.siblingAtColumn(1).data().toString();
            std::string test1 = test.toStdString();
            ui->textBrowser->setText(test);

            //xmlFile->open();

            //    qDebug()<<index.row();
            //    qDebug()<<index.column();

            ui->treeView->selectionModel()->select(index, QItemSelectionModel::Select | QItemSelectionModel::Rows);
            //ui->treeView
        }
        else
        { //ui->treeView->indexBelow(index);
            qDebug() << index.data().toString();
            qDebug() << "NOTTTT Selected";
            ui->treeView->selectionModel()->select(index, QItemSelectionModel::Deselect | QItemSelectionModel::Rows);
        }
        index = ui->treeView->indexBelow(index);
    }
    //qDebug()<<value;

    // qDebug()<<index.row();
    // qDebug()<<index.column();

    /*while (index.isValid()){
        if(index.data().toString()=="TCP"){
            xmlFile->open();
        }
        else qDebug()<<"not ok";
        ui->treeView->indexBelow(index);
    }*/
}
test_protocol MainWindow::getProtocolId(std::string protocol){
    //enum test_protocol { NONE = 0, TCP, UDP, ARP, ICMP, IPv4, DHCP, SOMEIP};
    if(protocol == "TCP"){
           return TCP;
       }else
           if(protocol == "UDP"){
               return UDP;
           }else
               if(protocol == "ARP"){
                   return ARP;
               }else
                   if(protocol == "IPv4"){
                       return IPV4;
                   }else
                       if(protocol == "DHCP"){
                           return DHCP;
                       }else
                           if(protocol == "SOMEIP"){
                           return SOMEIP;}else

                           if(protocol == "ICMPv4"){
                               return ICMP;}else
                               if(protocol == "DHCPv4"){
                                   return DHCP;}else
                                   if(protocol == "SOMEIPSRV"){
                                       return SOMEIP;}

                       else{
                           return NONE;
                       }
}
void MainWindow::on_treeView_clicked(const QModelIndex index)
{
    // xmlFile->on_treeView_clicked(index);
    // QString test =index.data().toString();
    ui->treeView->model()->data(index, Qt::CheckStateRole) = Qt::ItemIsAutoTristate;
    if (ui->treeView->model()->data(index, Qt::CheckStateRole) == Qt::Checked)
    {
        QString test = index.siblingAtColumn(1).data().toString();
        std::string test1 = test.toStdString();
        ui->textBrowser->setText(test);
         qDebug() <<"Checked test name="<<index.siblingAtColumn(0).data().toString()<<",Description="<<index.siblingAtColumn(1).data().toString();

         std::string testName = index.siblingAtColumn(0).data().toString().toStdString();
         int           testId   = trig(QString(testName.c_str()));
          std::string         testCategory =testName.substr(0,testName.find("0"));
           std::string         testDescription = index.siblingAtColumn(1).data().toString().toStdString();
            std::string         expectedResult = "Passed";
             std::string         testResultCause = "";
             time_t rawtime;
               struct tm * timeinfo;
               char buffer[80];

               time (&rawtime);
               timeinfo = localtime(&rawtime);

               strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
               std::string str(buffer);
              std::string         timestamp=str;

               test_protocol   ethernetProtocol =  getProtocolId(testName.substr(0,testName.find("_")));
               result          testResult = NOT_EXECUTED;
               bool            resultIsReady = false;

         // qDebug() <<"Unchecked test name="<<index.siblingAtColumn(0).data().toString()<<",Description="<<index.siblingAtColumn(1).data().toString();
     testDataCreator.addTestData(testId,testName,testCategory,testDescription,testResultCause,timestamp,ethernetProtocol,testResult,resultIsReady);

    }
    else
    {
        ui->textBrowser->clear();
    }

    if (ui->treeView->model()->data(index, Qt::CheckStateRole) == Qt::Unchecked)
    {
        QString test = index.siblingAtColumn(1).data().toString();
        std::string test1 = test.toStdString();
        ui->textBrowser->setText(test);
        std::string testName = index.siblingAtColumn(0).data().toString().toStdString();
        int             testId = trig(QString(testName.c_str()));
         testDataCreator.removeTestData(testId);

            /*
     *
     * int             testId,
                                    std::string         testName,
                                    std::string         testCategory,
                                    std::string         testDescription,
                                    std::string         expectedResult,
                                    std::string         testResultCause,
                                    std::string         timestamp,
                                    test_protocol   ethernetProtocol,
                                    result          testResult,
                                    bool            resultIsReady
     * */


    }
    else
    {
        ui->textBrowser->clear();
    }
    QStandardItem *l_itemClicked = xmlFile->model->itemFromIndex(index);

    if (!l_itemClicked->rowCount())
        return; // clicked item has no children

    for (int i = 0; i < l_itemClicked->rowCount(); ++i)
    {
        QStandardItem *l_child = l_itemClicked->child(i);
        bool l_isChecked = l_child->checkState() == Qt::Checked;

        if (l_child->isCheckable())
            l_child->setCheckState(l_isChecked ? Qt::Unchecked : Qt::Checked);
    }

    // QFuture<void> future = QtConcurrent::run(aFunction);
    /*if(!index.isValid())
            return;

        if(index.column() != 0)
            return;


        ui->treeView->update(index);
  // QVariant data = ui->treeView->model()->data(index, Qt::CheckStateRole);
   //QString text = data.toString();
//qDebug()<< text;
if (ui->treeView->model()->data(index, Qt::CheckStateRole) == Qt::Checked)
    {   //qDebug()<< ui->treeView->model()->data(index);
        qDebug() << "Selected";
        qDebug()<<index.data().toString();
        //QString test= index.data().toString();


        //    qDebug()<<index.row();
        //    qDebug()<<index.column();

        ui->treeView->selectionModel()->select(index, QItemSelectionModel::Select | QItemSelectionModel::Rows);
        //ui->treeView
    }
else
    {
        qDebug() << "NOTTTT Selected";
        ui->treeView->selectionModel()->select(index, QItemSelectionModel::Deselect | QItemSelectionModel::Rows);
    }
   //qDebug()<<value;

  // qDebug()<<index.row();
  // qDebug()<<index.column();*/
}

// register_connection_update(on_connection_update);
void MainWindow::on_writeXml_clicked()
{ //QModelIndex index;

    //xmlFile->writeXml();
    QString filename = QFileDialog::getSaveFileName(this, "Save Xml", ".", "Xml files (*.xml)");
    QFile file(filename);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        qDebug() << "Error saving XML file.";
        file.close();
        return;
    }
    QModelIndex index1;
    index1 = ui->treeView->indexAt(ui->treeView->rect().topLeft());
    //qDebug()<<index1.data().toString();
    // QString test = index1.data().toString();
    QDomDocument xml("test");
    QDomElement root = xml.createElement("Test_Done");
    /*  xml.appendChild(root);
    QDomElement tagname = xml.createElement("test1");
    root.appendChild(tagname);
    QDomText textname= xml.createTextNode("Test");
    tagname.appendChild(textname);*/
    //root.setAttribute("test","tcp");

    while (index1.isValid())
    {
        if (ui->treeView->model()->data(index1, Qt::CheckStateRole) == Qt::Checked)
        { //qDebug()<< ui->treeView->model()->data(index);

            //qDebug()<<index.data().toString();
            QString test1 = index1.data().toString();
            xml.appendChild(root);
            QDomElement tagname = xml.createElement(test1);
            root.appendChild(tagname);
            QDomText textname = xml.createTextNode("Test");
            //root.setAttribute("tcp","tcp");
            tagname.appendChild(textname);
        }

        else
        {
            qDebug() << "pass";
        }
        index1 = ui->treeView->indexBelow(index1);

        //QModelIndex index = model->currentIndex();*/
        //xmlFile->writeXml();
        //root.setAttribute(test,"tcp");
        /* xml.appendChild(root);
    QDomElement tagname = xml.createElement(test);
    root.appendChild(tagname);
    QDomText textname= xml.createTextNode("Test"); //john
    //root.setAttribute("tcp","tcp");
    tagname.appendChild(textname);*/
    }
    QTextStream output(&file);
    output << xml.toString();
    file.close();
    //xmlFile->on_actionSave_triggered();
}

/*void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(undoAct);
    editMenu->addAction(redoAct);
    editMenu->addSeparator();
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);
    editMenu->addSeparator();

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);
    formatMenu = editMenu->addMenu(tr("&Format"));
        formatMenu->addAction(boldAct);
        formatMenu->addAction(italicAct);
        formatMenu->addSeparator()->setText(tr("Alignment"));
        formatMenu->addAction(leftAlignAct);
        formatMenu->addAction(rightAlignAct);
        formatMenu->addAction(justifyAct);
        formatMenu->addAction(centerAct);
        formatMenu->addSeparator();
        formatMenu->addAction(setLineSpacingAct);
        formatMenu->addAction(setParagraphSpacingAct);
    }*/

void MainWindow::createActionsAndConnections()
{
    openAction = new QAction(tr("&Open..."), this);
    openAction->setShortcuts(QKeySequence::Open);
    QObject::connect(openAction, &QAction::triggered, this, &MainWindow::initXmlFile);
}
void MainWindow::initXmlFile()
{
    filePath = QFileDialog::getOpenFileName(NULL, "chose", "C://Users//uid1321//Desktop//rep//frontend-com-module//IHM-module//doc//Ethernet.xml");
    QFile file(filePath);
    if (!file.open(QIODevice::ReadWrite))
    {
        qDebug() << "Error!!!can't open file:" << filePath;
        return;
    }
    doc.clear();
    if (!doc.setContent(&file))
    {
        qDebug() << "Error!!!QDomDocument can't set content";
        file.close();
        return;
    }
    file.close();
}
void MainWindow::open()
{
    filePath = QFileDialog::getOpenFileName(NULL, "chose", "../doc/Ethernet.xml");
    QFile file(filePath);
    if (!file.open(QIODevice::ReadWrite))
    {
        qDebug() << "Error!!!can't open file:" << filePath;
        return;
    }
    doc.clear();
    if (!doc.setContent(&file))
    {
        qDebug() << "Error!!!QDomDocument can't set content";
        file.close();
        return;
    }
    file.close();
}
void MainWindow::createActions()
{
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    QAction *openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, &QAction::triggered, this, &MainWindow::open);
    fileMenu->addAction(openAct);
}

void MainWindow::Reset_TreeView()
{
    xmlFile->open();
    xmlFile->startParse();
     ui->progressBar->setValue(0) ;
     ui->treeView->setIndentation(30);

}

void MainWindow::on_actionreset_triggered()
{
    xmlFile->open();
    xmlFile->startParse();
     ui->progressBar->setValue(0) ;
     ui->treeView->setIndentation(30);

}
