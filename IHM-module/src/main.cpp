#include "mainwindow.h"

#include <QApplication>
#include "ETLComModule.h"
#include <QFile>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QFile styleSheetFile(".\\MailSy.qss");
    styleSheetFile.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(styleSheetFile.readAll());
    a.setStyleSheet(styleSheet);

    MainWindow w;
    w.show();


    return a.exec();

}
