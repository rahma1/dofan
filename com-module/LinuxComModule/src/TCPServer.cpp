//library used
#include <stdio.h>
#include <string.h> //strlen
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>    //close
#include <arpa/inet.h> //close
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros
#include <thread>
#include "TCPServer.h"
#include "ApiManager.h"

int opt = TRUE;
int master_socket, addrlen, new_socket, client_socket[30],
    max_clients = 30, activity, i, valread, sd;
int max_sd;
struct sockaddr_in address;

uint8_t buffer[256]; //data buffer of 256
//set of socket descriptors
fd_set readfds;

test_result_frame_t test_result;

TCPServer::TCPServer()
{
}

TCPServer::~TCPServer()
{
}

void TCPServer::start()
{
    printf("starting\n");
    std::thread t1(&TCPServer::ServerStart, this);
    t1.join();
}

void TCPServer::ServerStart()
{
    //initialise all client_socket[] to 0 so not checked
    for (i = 0; i < max_clients; i++)
    {
        client_socket[i] = 0;
    }

    //create a master socket
    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //set master socket to allow multiple connections ,
    //this is just a good habit, it will work without this
    if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,
                   sizeof(opt)) < 0)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    //type of socket created
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(INADDR_ANY); //change it to your adress
    address.sin_port = htons(Port);

    //bind the socket to your Adress port 9876
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Listener on port %d \n", Port);

    //try to specify maximum of 3 pending connections for the master socket
    if (listen(master_socket, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //accept the incoming connection
    addrlen = sizeof(address);
    puts("Waiting for connections ...");

    while (TRUE)
    {
        //clear the socket set
        FD_ZERO(&readfds);

        //add master socket to set
        FD_SET(master_socket, &readfds);
        max_sd = master_socket;

        //add child sockets to set
        for (i = 0; i < max_clients; i++)
        {
            //socket descriptor
            sd = client_socket[i];

            //if valid socket descriptor then add to read list
            if (sd > 0)
                FD_SET(sd, &readfds);

            //highest file descriptor number, need it for the select function
            if (sd > max_sd)
                max_sd = sd;
        }

        //wait for an activity on one of the sockets , timeout is NULL ,
        //so wait indefinitely
        activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);

        if ((activity < 0) && (errno != EINTR))
        {
            printf("select error");
        }

        //If something happened on the master socket ,
        //then its an incoming connection
        if (FD_ISSET(master_socket, &readfds))
        {
            if ((new_socket = accept(master_socket,
                                     (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }

            //inform user of socket number - used in send and receive commands
            printf("New connection , socket fd is %d , ip is : %s , port : %d \n ", new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));

            //add new socket to array of sockets
            for (i = 0; i < max_clients; i++)
            {
                //if position is empty
                if (client_socket[i] == 0)
                {
                    client_socket[i] = new_socket;
                    printf("Adding to list of sockets as %d\n", i);

                    break;
                }
            }
        }

        //else its some IO operation on some other socket
        for (i = 0; i < max_clients; i++)
        {
            sd = client_socket[i];

            if (FD_ISSET(sd, &readfds))
            {
                //Check if it was for closing , and also read the
                //incoming message
                if ((valread = read(sd, buffer, sizeof(buffer))) == 0)
                {
                    //Somebody disconnected , get his details and print
                    getpeername(sd, (struct sockaddr *)&address,
                                (socklen_t *)&addrlen);
                    printf("Host disconnected , ip %s , port %d \n",
                           inet_ntoa(address.sin_addr), ntohs(address.sin_port));

                    //Close the socket and mark as 0 in list for reuse
                    close(sd);
                    client_socket[i] = 0;
                }
                else
                {
                    printf("Recieved from %s : ", inet_ntoa(address.sin_addr));
                    for (int i = 0; i < valread; i++)
                    {
                        printf("%02x, ", buffer[i]);
                    }
                    printf("\n");
                    uint16_t crc = (uint16_t)buffer[valread - 2] << 8 & buffer[valread - 1];
                    switch (buffer[0])
                    {
                    case 0x01:
                    {
                        send(sd, buffer, sizeof(buffer), 0);
                        break;
                    }
                    case 0x03:
                    {
                        internal_protocol_frame_t *rf = (internal_protocol_frame_t *)buffer;
                        printf("protocol frame recieved: %02x, %02x, %04x, %04x, %04x, %04x\n", rf->Opcode, rf->TC8ID, rf->Test, rf->Api, rf->Reserved, rf->Checksum);
                        test_result = APIrunner(*rf);
                        printf("result frame sent to HMI %02x, %02x, %04x, %04x, %04x, %04x, %04x\n", test_result.Opcode, test_result.TC8ID, test_result.Test, test_result.Api, test_result.ErrorCode, test_result.Reserved, test_result.Checksum);
                        send(sd, (uint8_t *)&test_result, sizeof(test_result_frame_t), 0);
                        break;
                    }
                    case 0x04:
                    {
                        Heartbeat *hb = (Heartbeat *)buffer;
                        printf("Heartbeat frame recieved: %02x, %04x, %04x, %04x\n", hb->Opcode, hb->seq_num, hb->Reserved1, hb->Checksum);
                        hb->seq_num++;
                        hb->Checksum = gen_crc16((uint8_t *)hb, sizeof(Heartbeat) - 2);
                        send(sd, (uint8_t *)hb, sizeof(Heartbeat), 0);

                        break;
                    }
                    default:
                        break;
                    }
                    bzero(buffer, sizeof(buffer));
                }
            }
        }
    }
}
