/**
 *  Copyright (C) [2021] Focus Corporation
 *
 *  \file   parameetr_configuration.h
 *  @brief 
 *  \author Abderrahim Jamaoui
 *
 *  \addtogroup TC8-ARP
 *  \{
 */

#ifndef PARA_CONF
#define PARA_CONF

#include <stdint.h> /* uint8_t */
#include <string.h>
/*#define DUT_HW_Address "B8:27:EB:9C:25:5D"

#define DUT_IP_Address "169.254.159.220"

#define Tester_HW_Address "08:00:27:38:BD:1D"  

#define Tester_IP_Address "169.254.159.221"*/


/* User defined configuration parameters for IUT */
#define DYNAMIC_ARP_CACHETIMEOUT            1
#define ARP_TOLERANCETIME                   1

/* User defined configuration parameters for TESTER  */
#define HOST_1_IP                           "192.168.20.83"
#define DIface_0_IP                         "192.168.20.117"
#define ParamListenTime                     2
#define MAC_ADDR1                           "08:00:27:CD:64:F1"
#define mac_addr1                           "08:00:27:cd:64:f1"
#define MAC_ADDR2                           "08:00:27:CD:64:F2"
#define mac_addr2                           "08:00:27:cd:64:f2"
#define MAC_ADDR3                           "08:00:27:CD:64:F3"
#define mac_addr3                           "08:00:27:cd:64:f3"
#define DIFACE_O_MAC_ADDR                   "B8:27:EB:3B:17:E9"
#define diface_0_mac_addr                   "b8:27:eb:3b:17:e9"
#define ARBIT_MAC_ADDR                      "12:34:56:78:90:00"
#define ARP_HARDWARE_TYPE_UNKNOWN           0x0002
#define UNKNOWN_HW_ADDR_LEN                 7
#define ARP_PROTOCOL_UNKNOWN                0x1234
#define UNKNOWN_PROTCOL_ADDR_LEN            5
#define ARP_TOLERANCE_TIME                  1
#define ETHERNET_ADDR_LEN                   0x06
#define ARP_HARDWARE_ETHERNET               0x01
#define ARP_PROTOCOL_TYPE                   0x0800
#define ARP_PROTOCOL_IP                     0x04
#define OPERATION_REQUEST                   0x01
#define all_zeroes                          "00:00:00:00:00:00"
#define ETHERNET_BROADCAST_ADDR             "ff:ff:ff:ff:ff:ff"
#define IP_FIRST_UNUSED_ADDR_INTERFACE_1    "192.168.100.200"
#define OPERATION_RESPONSE                  0x02

/*
typedef struct 
{
    uint8_t HOST_1_IP[16];
    uint8_t DIface_0_IP[16];
    uint8_t ParamListenTime;
    uint8_t MAC_ADDR1[18];
    uint8_t MAC_ADDR2[18];
    uint8_t MAC_ADDR3[18];
    uint8_t DIFACE_O_MAC_ADDR[18];
    uint8_t ARBIT_MAC_ADDR[18];
    uint16_t ARP_HARDWARE_TYPE_UNKNOWN;
    uint8_t UNKNOWN_HW_ADDR_LEN;
    uint16_t ARP_PROTOCOL_UNKNOWN;
    uint8_t UNKNOWN_PROTCOL_ADDR_LEN;
    uint8_t ARP_TOLERANCE_TIME;
    uint8_t ETHERNET_ADDR_LEN;
    uint8_t ARP_HARDWARE_ETHERNET;
    uint16_t ARP_PROTOCOL_TYPE;
    uint8_t ARP_PROTOCOL_IP;
    uint8_t OPERATION_REQUEST;
    uint8_t all_zeroes[18];
    uint8_t ETHERNET_BROADCAST_ADDR[18];
    uint8_t IP_FIRST_UNUSED_ADDR_INTERFACE_1[16];
    uint8_t OPERATION_RESPONSE;
}parameters_list;

parameters_list global_param_list;  

void init_param_list();
int set_config(parameters_list param_list);
parameters_list get_config();*/




#endif // PARA_CONF