#ifndef ICOMMODULE_H
#define ICOMMODULE_H

#include <functional>
#include <list>

//#define CONF_FILE_PATH "\\..\\IComModule\\conf\\testcase.conf"
#define CONF_FILE_PATH "\\..\\..\\com-module\\IComModule\\conf\\testcase.conf"

using namespace std::placeholders;

//start frame
struct com_initializer_t
{
    uint8_t OpCode=0x01;
    uint16_t Checksum=0;
};

//Ethernet Configuration
struct GlobalConfig
{
    uint8_t Opcode=0x02;
    //std::string Dut_address="";
    char Dut_address[20];
    int Dut_Port=0;
    uint16_t Checksum=0;
    //to be added other needed parameter
};



//internal_protocol_frame_struct
struct internal_protocol_frame_t
{
    uint8_t Opcode=0x03;
    uint8_t TC8ID=0x01;
    uint16_t Test=0;
    uint16_t Api=0;
    uint16_t Reserved=0x0000;
    uint16_t Checksum=0;
};

//test_result_frame_struct
struct test_result_frame_t
{
    uint8_t Opcode;
    uint8_t TC8ID;
    uint16_t Test;
    uint16_t Api;
    uint16_t ErrorCode;
    uint16_t Reserved;
    uint16_t Checksum;
};
//Heartbeat struct
struct Heartbeat_t
{
    uint8_t Opcode=0x04;
    uint16_t seq_num=0x0000;
    uint16_t Reserved1=0x0000;
    uint16_t Checksum=0;
};

struct testID_APIID_conf_t
{
  int Test_ID;
  int API_ID;
};

std::vector<testID_APIID_conf_t> global_config_vector;


class IComModule
{
public:
    //_________________________________________
    // ::: Configuration and initialization :::

    /**********************************************
     * **************Uart config*******************
     * ********************************************
     */


    /*!
    \brief Open the serial port
    \param Device : Port name (COM1, COM2, ... for Windows )
    \param Bauds : Baud rate of the serial port.
    \n Supported baud rate for Windows :
            - 56000
            - 57600
            - 115200
            - 128000
            - 256000
    \return 0 success
    \return -1 device not found
    \return -2 error while opening the device
    \return -3 error while getting port parameters
    \return -4 Speed (Bauds) not recognized
    \return -5 error while writing port parameters
    */
    virtual int openPort(std::string &Device, const unsigned int &Bauds);

    /*!
    \brief Close the connection with the current device
    */
    virtual int closePort();

     /*********************************************
     * ************Ethernet config*****************
     * *******************************************/

    int ConnectToTesterTh(const std::string &Address, const int &Port);

    /*!
    \brief Establish Connection with Server
    \param Adress : "192.168.1.1"
    \param Port : 5001
    \return 0 Success
    \return -1 Error WSAStartup failed
    \return -2 Error create socket failed
    \return -3 Error connection failed
    */
    virtual int ConnectToTester(const std::string &Address, const int &Port);


    /*!
    \brief Start Test
    \param protocolID
    \param TestID
    \return 1 Success
    \return -1 Error Send Failed
    */
    virtual uint16_t StartTest(uint16_t &TestID);



    /*!
     * \brief SetGlobalConfig
     * \param GlobalConfig {Tester_address ; tester_port, DUT_adress, DUT_port, MaxTestTimeout,... }
     * \return 0 success
     * \return -1 fail
     */
    virtual int SetGlobalConfig(GlobalConfig Globalconf);





    /*!
    \brief Disconnect from Server
    \return 0 Success
    \return -1 Error Socket Shutdown failed
    */
    virtual int DisConnect();

    //Connection Register
    /*!
     \brief Register Tester Connection
     \param pointer to function
     \return 0 : status connected
     \return -1  status not connected
     */
    virtual void RegisterTesterConnection(std::function<void(bool)> on_connection_update);

    /*!
     \brief Register DUT Connection
     \param pointer to function
     \return 0 : status connected
     \return -1  status not connected
     */
    virtual void RegisterDutConnection(std::function<void(bool)> on_connection_update);

private:

    /*!
     * \brief gen_crc16  function to calculate checksum f a given data
     * \param data : data to calculate its checksum
     * \param size : size of data
     * \return checksum
    */
    uint16_t gen_crc16(const uint8_t *data, uint16_t size);

    /*!
    \brief Recieve Data
    \return 0 Success
    \return -1 Error Failing while Recieving
    */
    virtual void Recieve();

    /*!
     * \brief HeartBeat
     * \param Address
     * \param Port
     * \return
     */
    //virtual int HeartBeat();

    virtual void Send();

    virtual uint16_t GetTestResult();

    virtual int load_config_file(std::string filepath, std::vector<testID_APIID_conf_t> &testID_APIID_conf_vector);

    virtual int get_API_ID(int TestID, std::vector<testID_APIID_conf_t> &_testID_APIID_conf_vector);

    virtual void HeartBeat();

};

#endif // ICOMMODULE_H
