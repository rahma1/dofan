#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "IComModule.h"
#include <QtWidgets>

class Client : public IComModule
{
};


Client client;



MainWindow::MainWindow(QWidget *parent)
    : QDialog(parent)
    , Adress(new QLineEdit(tr("192.168.6.135")))
    , port(new QLineEdit(tr("9876")))
    , btn_Connect(new QPushButton(tr("Connect")))
    , btn_Disconnect(new QPushButton(tr("Disconnect")))
    , Status(new QLabel(tr("Status:")))
    , starttest(new QPushButton(tr("Start Test")))
    , dialog(new QPlainTextEdit)
    , GetResult(new QPushButton(tr("Get Result")))
    , StartCompaign(new QPushButton(tr("Start Compaign")))
    , StartProtocol(new QPushButton(tr("Start Compaign by Protocol")))
{
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    auto hostLabel = new QLabel(tr("&Server Adress:"));
    hostLabel->setBuddy(Adress);
    auto portLabel = new QLabel(tr("&port:"));
    portLabel->setBuddy(port);

    statusLabel = new QLabel(tr("This test requires that you run the "
                                "TCP Server as well."));

    /*btn_Disconnect->setDefault(true);
    btn_Disconnect->setEnabled(false);
    starttest->setDefault(true);
    starttest->setEnabled(false);*/

    auto quitButton = new QPushButton(tr("Quit"));

    auto buttonBox = new QDialogButtonBox;
    buttonBox->addButton(btn_Connect, QDialogButtonBox::ActionRole);
    buttonBox->addButton(starttest, QDialogButtonBox::ActionRole);
    buttonBox->addButton(btn_Disconnect, QDialogButtonBox::ActionRole);
    buttonBox->addButton(quitButton, QDialogButtonBox::RejectRole);

    connect(btn_Connect, &QAbstractButton::clicked, this, &MainWindow::Establish_Connection);
    connect(starttest, &QAbstractButton::clicked, this, &MainWindow::teststart);
    //connect(GetResult,&QAbstractButton::clicked,this,&MainWindow::Getresult);
    connect(StartProtocol,&QAbstractButton::clicked,this,&MainWindow::startbyProtocol);
    connect(StartCompaign,&QAbstractButton::clicked,this,&MainWindow::CompaignStart);
    connect(btn_Disconnect, &QAbstractButton::clicked, this, &MainWindow::Disconnection);
    connect(quitButton, &QAbstractButton::clicked, this, &QWidget::close);

    QGridLayout *mainLayout = nullptr;
    if (QGuiApplication::styleHints()->showIsFullScreen() || QGuiApplication::styleHints()->showIsMaximized())
    {
        auto outerVerticalLayout = new QVBoxLayout(this);
        outerVerticalLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Ignored, QSizePolicy::MinimumExpanding));
        auto outerHorizontalLayout = new QHBoxLayout;
        outerHorizontalLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::MinimumExpanding, QSizePolicy::Ignored));
        auto groupBox = new QGroupBox(QGuiApplication::applicationDisplayName());
        mainLayout = new QGridLayout(groupBox);
        outerHorizontalLayout->addWidget(groupBox);
        outerHorizontalLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::MinimumExpanding, QSizePolicy::Ignored));
        outerVerticalLayout->addLayout(outerHorizontalLayout);
        outerVerticalLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Ignored, QSizePolicy::MinimumExpanding));
    }
    else
    {
        mainLayout = new QGridLayout(this);
    }
    mainLayout->addWidget(hostLabel, 0, 0);
    mainLayout->addWidget(Adress, 0, 1);
    mainLayout->addWidget(portLabel, 1, 0);
    mainLayout->addWidget(port, 1, 1);
    mainLayout->addWidget(statusLabel, 2, 0, 1, 2);
    mainLayout->addWidget(Status, 3, 0, 1, 2);
    mainLayout->addWidget(dialog, 4, 0, 1, 1);
    //mainLayout->addWidget(GetResult, 4, 1, 0, 2);
    mainLayout->addWidget(StartCompaign, 4, 1,1, 2);
    mainLayout->addWidget(StartProtocol, 4, 1, 5, 2);
    mainLayout->addWidget(buttonBox, 5, 0, 1, 2);

    setWindowTitle(QGuiApplication::applicationDisplayName());
    port->setFocus();
    //! [5]
    //!
}

MainWindow::~MainWindow()
{
}
void MainWindow::updatelabel(QString text)
{
    Status->setText(text);
}

void MainWindow::updateplaintextlabel(QString text)
{
    dialog->moveCursor(QTextCursor::End);
    dialog->insertPlainText(text);
    dialog->moveCursor(QTextCursor::End);
}

void MainWindow::parse_result(uint16_t result)
{
    std::string final_state;
        switch (result)
        {
        case 0x0000:
        {
            final_state= "test state: success\n";
            break;
        }
        case 0x0001:
        {
            final_state= "test state: failed \n";
            break;
        }
        case 0x0002:
        {
            final_state="Timeout\n";
        }
        }
    updateplaintextlabel(QString::fromStdString(final_state));
}

void MainWindow::on_connection(bool status)
{
    if (status == true)
        updatelabel(QString::fromStdString("Status: Connected"));
    else if (status ==false)
        updatelabel(QString::fromStdString("Status: Disconnected"));
}



int MainWindow::Establish_Connection()
{
    int iResult = client.EthConnect(Adress->text().toLocal8Bit().constData(), port->text().split(" ")[0].toInt());
    if (iResult == 0)
    {
        /*btn_Disconnect->setDefault(true);
        btn_Disconnect->setEnabled(true);
        btn_Connect->setDefault(true);
        btn_Connect->setEnabled(false);
        starttest->setDefault(true);
        starttest->setEnabled(true);*/
    }
    else
        printf("%d\n", iResult);
    client.RegisterTesterConnection(bind(&MainWindow::on_connection, this, _1));
    return 0;
}

int MainWindow::Disconnection()
{
    //ETL_Com_Module client;
    client.DisConnect();
    /*btn_Disconnect->setDefault(true);
    btn_Disconnect->setEnabled(false);
    btn_Connect->setDefault(true);
    btn_Connect->setEnabled(true);
    starttest->setDefault(true);
    starttest->setEnabled(false);*/
    return 0;
}

int MainWindow::teststart()
{
    uint16_t x = 78;
    uint16_t ret=client.StartTest(x);
    updateplaintextlabel(QString::fromStdString("Client:start sending\n"));
    parse_result(ret);

    return 0;
}


int MainWindow::CompaignStart()
{
    std::list<uint16_t> testlist;
    testlist.push_back(0x0001);
    testlist.push_back(0x0003);
    client.startCompain(testlist);
    return 0;
}

int MainWindow::startbyProtocol()
{
    uint16_t x=0xffff;
    client.startCompaignbyProtocol(x);
    return 0;
}
