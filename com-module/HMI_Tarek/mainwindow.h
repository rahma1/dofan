#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDialog>
#include <QTreeView>
#include <QPlainTextEdit>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QTcpSocket;
class QPlainTextEdit;
QT_END_NAMESPACE

class MainWindow : public QDialog
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    int Establish_Connection();
    int teststart();
    int Disconnection();
    void updatelabel(QString text);
    void updateplaintextlabel(QString text);
    int CompaignStart();
    int startbyProtocol();
    void on_connection(bool status);
    void parse_result(uint16_t result);
private:
    Ui::MainWindow *ui;
    QLineEdit *Adress = nullptr;
    QLineEdit *port = nullptr;
    QLabel *statusLabel = nullptr;
    QPushButton *btn_Connect = nullptr;
    QPushButton *btn_Disconnect = nullptr;
    QLabel *Status=nullptr;
    QPushButton *starttest=nullptr;
    QTreeView *tests=nullptr;
    QPlainTextEdit *dialog=nullptr;
    QPushButton *GetResult=nullptr;
    QPushButton *StartCompaign=nullptr;
    QPushButton *StartProtocol=nullptr;
};
#endif // MAINWINDOW_H
